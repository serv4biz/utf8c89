#include <stdio.h>
#include <string.h>

#include "../utf8.h"

int main() {
	u8str_t *str = u8str_new("Hello world สวัสดี");

	printf("%s = %d\n", u8str_to_string(str),
			(int) strlen((const char*) u8str_to_string(str)));
	printf("Length : %d\n", (unsigned int) u8str_length(str));

	u8str_free(&str);

	printf("%d",strlen("test"));
	return 0;
}
